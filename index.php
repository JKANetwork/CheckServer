<?php
if (isset($_GET['logout'])){session_start(); session_destroy();} //Logout
if (!file_exists(__DIR__ .'/config/config.php')){
    header('location: install.php');
}
require_once 'functions.php';




$db_conn = dbconn();

//Calculemos estadísticas
$servs = dbw_query_fetch_array($db_conn,"SELECT COUNT(*) FROM SERVERS")[0];

//Sacar datos de cada servidor
$serverssql = dbw_query($db_conn,"SELECT ID_SERV FROM SERVERS"); //Todos los ID_SERV
$numrows['all'] = dbw_num_rows($db_conn,$serverssql);
$numrows['on'] = 0;
$numrows['enabled'] = dbw_query_fetch_array($db_conn,"SELECT COUNT(*) FROM SERVERS WHERE `Enabled`=1")[0];

$SEnabled = 0;
$STotal = 0;
$SActive = 0;

while ($id = dbw_fetch_array($db_conn,$serverssql)){
    $server = serverData($id['ID_SERV']);
    if ($server['Online']){$numrows['on']++;}
    $servers[$id['ID_SERV']] = $server; //Insert data array

    $STotal += $server['STotal'];
    $SEnabled += $server['SEnabled'];
    $SActive += $server['SActive'];
}
$numrows['off'] = $numrows['all'] - $numrows['on'];

$numrows['allsensors'] = $STotal + $numrows['all'];
$numrows['activesensors'] = $SActive + $numrows['on'];

if (!isset($servers)){
    $servers = NULL;
}
renderPage('index.twig',array('servers' => $servers,'numrows' => $numrows));
