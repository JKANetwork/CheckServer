<?php
//File for doing some Ajax operations as toggle perms of things of servers
if (file_exists(__DIR__ .'/config/config.php')){
    require_once __DIR__ .'/config/config.php';
}
require_once __DIR__ .'/functions.php';

switch ($_POST['do']){
    case 'toggleperm':
        $perm = $_POST['perm'];
        $ID_U = (int)trim($_POST['ID_U']);
        if (getPerm('PE_admin') || ($perm == 'SendMail' && $ID_U = $_SESSION['user']['ID_U'])){ //Solo el propio usuario puede cambiar la casilla de recibir correo aparte.

            $resp = dbw_query_fetch_array(dbconn(),"SELECT ".$perm." FROM USERS WHERE ID_U='$ID_U'")[0];
            if ($resp){ //Si está, quitarle, y viceversa
                dbw_query(dbconn(),"UPDATE USERS SET ".$perm."=0 WHERE ID_U='$ID_U'");
            }else{
                dbw_query(dbconn(),"UPDATE USERS SET ".$perm."=1 WHERE ID_U='$ID_U'");
            }
        }
    break;
    case 'togglesvc':
        if (getPerm('PE_editserv')){
            $name = $_POST['name'];
            $ID_SERV = (int)$_POST['ID_SERV'];
            $resp = dbw_query_fetch_array(dbconn(),"SELECT * FROM S_SERVICES WHERE ID_SERV='$ID_SERV' AND `Name`='$name'");
            if ($resp['Enabled']){ //Si está, quitarle, y viceversa
                dbw_query(dbconn(),"UPDATE S_SERVICES SET `Enabled`=0 WHERE ID_SERV='$ID_SERV' AND `Name`='$name'");
            }else{
                dbw_query(dbconn(),"UPDATE S_SERVICES SET `Enabled`=1 WHERE ID_SERV='$ID_SERV' AND `Name`='$name'");
            }
        }
    break;
    case 'togglesrv':
        if (getPerm('PE_editserv')){
            $ID_SERV = (int)$_POST['ID_SERV'];
            $resp = dbw_query_fetch_array(dbconn(),"SELECT * FROM SERVERS WHERE ID_SERV='$ID_SERV'");
            if ($resp['Enabled']){ //Si está, quitarle, y viceversa
                dbw_query(dbconn(),"UPDATE SERVERS SET `Enabled`=0 WHERE ID_SERV='$ID_SERV'");
                dbw_query(dbconn(),"DELETE FROM P_ALERTS WHERE ID_SERV='$ID_SERV'");
            }else{
                dbw_query(dbconn(),"UPDATE SERVERS SET `Enabled`=1 WHERE ID_SERV='$ID_SERV'");
            }
        }
    break;
    case 'updatesrv':
        if (getPerm('PE_editserv')){
            $ID_SERV = (int)$_POST['ID_SERV'];
            /*$resp = dbw_query_fetch_array(dbconn(),"SELECT * FROM SERVERS WHERE ID_SERV='$ID_SERV'");
            if ($resp['Enabled']){ //Si está, quitarle, y viceversa
                dbw_query(dbconn(),"UPDATE SERVERS SET `Enabled`=0 WHERE ID_SERV='$ID_SERV'");
            }else{
                dbw_query(dbconn(),"UPDATE SERVERS SET `Enabled`=1 WHERE ID_SERV='$ID_SERV'");
            }*/
            exec("php -f ".__DIR__ .'/cron/cron.php '.$ID_SERV);
            
            sendmsg('debug','updatesrv');
        }
    break;

    case 'deloldlogs':
        if (getPerm('PE_admin')){
            $time = time() - (15 * 24 * 60 * 60);
            dbw_query(dbconn(),"DELETE FROM S_HISTRAM WHERE `Timestamp` < '$time'");
            dbw_query(dbconn(),"DELETE FROM S_HISTPING WHERE `Timestamp` < '$time'");
            dbw_query(dbconn(),"DELETE FROM S_HISTSERVICES WHERE `Timestamp` < '$time'");
            dbw_query(dbconn(),"DELETE FROM S_HDDSTAT WHERE `Timestamp` < '$time'");
        }
}