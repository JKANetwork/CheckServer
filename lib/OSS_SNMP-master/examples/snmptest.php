#! /usr/bin/php
<?php
$ip = '127.0.0.1';
$community = 'public';
require_once( dirname( __FILE__ ) . '/../OSS_SNMP/SNMP.php' );

$host = new \OSS_SNMP\SNMP( $ip, $community );

echo "\nSystem information for {$ip}:\n\n";

print_r( $host->useSystem()->getAll() );

echo "\n\n";


echo "\nNumber of interfaces on {$ip}: " . $host->useIface()->numberofInterfaces() . "\n\n";

echo "ID:  Name  - Descrition - Type - Admin/Operational State\n\n";

foreach( $host->useIface()->names() as $id => $name )
{
	echo "{$id}: {$name} - {$host->useIface()->descriptions()[$id]} - {$host->useIface()->types(1)[$id]}"
		. " - {$host->useIface()->adminStates(1)[$id]}/{$host->useIface()->operationStates(1)[$id]}\n";
}

echo "\n";

