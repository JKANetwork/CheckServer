#! /usr/bin/php
<?php
$ip = '127.0.0.1';
$community = 'public';
require_once( dirname( __FILE__ ) . '/../OSS_SNMP/SNMP.php' );

$host = new \OSS_SNMP\SNMP( $ip, $community );

echo "\nSystem information for {$ip}:\n\n";

print_r( $host->useSystem()->getAll() );

echo "\n\n";

exit( 0 );
