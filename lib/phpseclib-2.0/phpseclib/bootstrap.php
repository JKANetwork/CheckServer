<?php
/**
 * Bootstrapping File for phpseclib
 *
 * @license http://www.opensource.org/licenses/mit-license.html MIT License
 */

if (extension_loaded('mbstring')) {
    // 2 - MB_OVERLOAD_STRING
    if (ini_get('mbstring.func_overload') & 2) {
        throw new \UnexpectedValueException(
            'Overloading of string functions using mbstring.func_overload ' .
            'is not supported by phpseclib.'
        );
    }
}

require_once dirname(__FILE__).'/Net/SSH2.php';
    //require_once "lib/phpseclib-2.0/phpseclib/Crypt/AES.php";
    require_once dirname(__FILE__).'/Math/BigInteger.php';
    require_once dirname(__FILE__).'/Crypt/Base.php';
    require_once dirname(__FILE__).'/Crypt/Hash.php';
    require_once dirname(__FILE__).'/Crypt/Random.php';
    require_once dirname(__FILE__).'/Crypt/Rijndael.php';
    require_once dirname(__FILE__).'/Crypt/RSA.php';
