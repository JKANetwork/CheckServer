<?php
spl_autoload_register(function ($class) {
    // project-specific namespace prefix
    $prefix = 'Twig';
    // base directory for the namespace prefix
    $base_dir = __DIR__ . '/Twig/lib/Twig';
    // does the class use the namespace prefix?
    $len = strlen($prefix);
    if (strncmp($prefix, $class, $len) !== 0) {
        // no, move to the next registered autoloader
        return;
    }
    // get the relative class name
    $relative_class = substr($class, $len);
    // replace the namespace prefix with the base directory, replace namespace
    // separators with directory separators in the relative class name, append
    // with .php
    $file = $base_dir . str_replace('_', '/', $relative_class) . '.php';
    //echo $file;
    // if the file exists, require it
    if (file_exists($file)) {
        require $file;
    }else{
        echo "ERROR LOADING";
    }

});


/* Load twig var */
$loader = new Twig_Loader_Filesystem(__DIR__ . '/../assets/html/');
$twig = new Twig_Environment($loader, array(
    //'cache' => 'lib/Twig/cache',
    //'debug' => true,
));
//$twig->addExtension(new Twig_Extension_Debug());