## For < 1.0 version (Legacy sqlite3 and simpler version), go here:  https://gitlab.com/JKANetwork/CheckServer/tree/v0.9.4

# CheckServer

CheckServer is a PHP app that monitor (using cron) some parameters in servers, ping to it, services, and views simple stadistics.
It only needs PHP, MySQL, and for talking with Windows machines, Linux wmi-client package (I'm open to change with a PHP library that works)

Also has a index stadistics page for guests (Not logued)


# Requirements
- Linux (In Windows you can't see Windows machines, wmi mismatch from Linux)
- PHP 7.0+
- wmi-client and shell_exec package if you want to control Windows machines
- MySQL
- Cron

# Bugs
- Ping needs root because socket use in some distros, only will refresh in cron (Run php as root in cron).

# Install
Download last version from https://gitlab.com/JKANetwork/CheckServer/tags (Tags=Releases, master is not released quality code), unzip it and copy all to the folder in your hosting.

Go to browser, /install.php and follow the installer

For finishing it, you have to add a cron in your system for running checks. The check has to run using php the file cron.php. Example for Linux, in /etc/cron.d/checkserver:
```
*/5 * * * *	root	cd /srv/http/checkserver/cron/ && /usr/bin/php -f cron.php >/dev/null 2>&1
```


# TODO

- Allow http response views

- WMI support for Windows servers

- At future, read sensors in routers/switchs/SNMP

# Changelog

1.0.0 - First public version (Beta quality)

-Completly reworked code

-You can't update to this from previous versions, you have to reinstall it