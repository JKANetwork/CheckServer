<?php
session_start(); session_destroy(); //No session in install
require_once 'functions.php';
if (file_exists(dirname(__FILE__).'/config/config.php')){
    die("Ya está instalado. Borre el fichero /config/config.php si quiere reiniciar la instalación");
}

//Comprobaciones
if (!function_exists('socket_create') ){
	sendmsg('error','Debes activar el soporte para socket en el php.ini si quieres que pueda hacer ping.');
}
if (!is_writable(__DIR__ .'/config/') ){
	sendmsg('error','Debes dar permisos de escritura al directorio config');
}

if (isset($_POST['adminname'])){

    list($adminname,$adminmail,$adminname,$adminpass,$dbhost,$dbdata,$dbuser,$dbpass) = array(
        cleanData($_POST['adminname']),cleanData($_POST['adminmail']),cleanData($_POST['adminname']),cleanData($_POST['adminpass']),
        cleanData($_POST['dbhost']),cleanData($_POST['dbdata']),cleanData($_POST['dbuser']),cleanData($_POST['dbpass'])
    );

    $db_conn = dbw_connect('mysqli',$dbhost,$dbdata,$dbuser,$dbpass);
    if (!$db_conn){
        sendmsg('error','No se puede conetar a la base de datos');
        renderPage('install.twig');
        die();
    }

    dbw_multi_query($db_conn,file_get_contents(dirname(__FILE__).'/assets/sqlinstalldata.sql'),1); //1 is erase buffer then. (For insert)
    //Crear usuario admin
    dbw_query($db_conn,"INSERT INTO USERS (`Name`,`Password`,`Email`,`PE_admin`) VALUES('$adminname','x','$adminmail',1)");
    $id_u = dbw_last_id($db_conn);
    $password = hash("sha256",$id_u.$adminpass); //Password with salt (id_u)
    dbw_query($db_conn,"UPDATE USERS SET `Password`='$password' WHERE ID_U='$id_u'");
    //Create config file
    $configfile = "<?php
define('ENCPASSWD','".RandomString(12)."');
define('SGBD','mysqli');
define('DBSERVER','".$dbhost."');
define('DBUSER','".$dbuser."');
define('DBPASS','".$dbpass."');
define('DBDATABASE','".$dbdata."');
define('DEBUG',false); ?>";
    file_put_contents(dirname(__FILE__).'/config/config.php', $configfile);

    header("location: index.php");
    die("<p>Instalado. Dirigiéndote al <a href='index.php'>login</a>, gracias por usar CheckServer</p>");
}else{
    renderPage('install.twig');
}