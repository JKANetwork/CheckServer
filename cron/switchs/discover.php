<?php

require_once dirname( __FILE__ ) . '/../../lib/OSS_SNMP-master/SNMP.php'; //Load SNMP

$ip = $_GET['IP'];

$host = new \OSS_SNMP\SNMP( $ip, 'public' );

echo "\nSystem information for {$ip}:\n\n";

print_r( $host->useSystem()->getAll() );

echo "\n\n";